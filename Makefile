# You can get xml2rfc from http://xml.resource.org/ or on debian it's:
# # apt-get install xml2rfc
name = draft-ietf-nfsv4-acl-mapping

$(name).txt: $(name).xml
	unset DISPLAY; xml2rfc $(name).xml

html: $(name).xml
	unset DISPLAY; xml2rfc $(name).xml $(name).html
